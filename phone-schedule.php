<?php

/**
 * Phone Schedule for 1daytaxi
 *
 * Plugin Name:       Phone Schedule
 * Version:           1.0.0
 * Author:            shkuter
 */

// If this file is called directly, abort.
if ( !defined( 'WPINC' ) ) {
	die;
}

if ( ! defined( 'PS_PLUGIN_DIR' ) ) {
	define( 'PS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'PS_PLUGIN_URL' ) ) {
	define( 'PS_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

// Disable admin bar
add_filter('show_admin_bar', '__return_false');

// Assets
function ps_assets() {
    wp_register_script('phone-schedule', PS_PLUGIN_URL . '/js/phone-schedule.js', array( 'jquery' ) );
    wp_enqueue_script( 'phone-schedule' );
}
add_action( 'admin_enqueue_scripts', 'ps_assets' );

// Admin menu page
add_action( 'admin_menu', 'ps_admin_menu' );
function ps_admin_menu() {
	add_menu_page( __('Расписание'), __('Расписание'), 'manage_options', 'phone-schedule', 'ps_admin_page', 'dashicons-phone', 1  );
}
function ps_admin_page() {
    $options = get_option( 'ps_settings' );
    ?>
    <form method="post" action="options.php">
        <?php wp_nonce_field('update-options'); ?>
        <input type="hidden" name="action" value="update" />
        <input type="hidden" name="page_options" value="ps_settings" />
        <div class="wrap">
            <h1>Расписание</h1>
            <table class="form-table" style="width:100px;">
                <tbody>
                    <tr>
                        <td></td>
                        <td><b>Дневной телефон</b></td>
                        <td><b>Ночной телефон</b></td>
                    </tr>
                    <tr>
                        <th>Пн</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][1]" type="text" value="<?= $options['dayphone'][1] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][1]" type="text" value="<?= $options['nightphone'][1] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Вт</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][2]" type="text" value="<?= $options['dayphone'][2] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][2]" type="text" value="<?= $options['nightphone'][2] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Ср</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][3]" type="text" value="<?= $options['dayphone'][3] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][3]" type="text" value="<?= $options['nightphone'][3] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Чт</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][4]" type="text" value="<?= $options['dayphone'][4] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][4]" type="text" value="<?= $options['nightphone'][4] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Пт</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][5]" type="text" value="<?= $options['dayphone'][5] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][5]" type="text" value="<?= $options['nightphone'][5] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Сб</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][6]" type="text" value="<?= $options['dayphone'][6] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][6]" type="text" value="<?= $options['nightphone'][6] ?>" /></td>
                    </tr>
                    <tr>
                        <th>Вс</th>
                        <td><input id="dayphone" name="ps_settings[dayphone][7]" type="text" value="<?= $options['dayphone'][7] ?>" /></td>
                        <td><input id="nightphone" name="ps_settings[nightphone][7]" type="text" value="<?= $options['nightphone'][7] ?>" /></td>
                    </tr>
                    <tr>
                        <th><label for="code">Код для вставки:</label></th>
                        <td><input id="code" type="text" value="[ps_phone]" /></td>
                    </tr>
                </tbody>
            </table>
            <p><input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>"></p>
    </form>

    <!-- <pre><?php var_dump($options); ?></pre> -->
    <?php
}

// Shortcode for phones
add_shortcode( 'ps_phones', 'phones_func' );
function phones_func( $atts ){
    $options = get_option( 'ps_settings' );
    $day_number = intval(date('N'));
	return '<span class="night-phone phone"><span class="tel">' . $options['nightphone'][$day_number] . '</span></span> <span class="day-phone phone"><span class="tel">' . $options['dayphone'][$day_number] . '</span></span>';
}
?>