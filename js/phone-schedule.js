(function($) {
	var day = $('.day-phone');
	var night = $('.night-phone');
	var hour = new Date().getUTCHours() + 3;
	if ((hour >= 6) && (hour < 17)) {
					day.show();
					night.hide();
	} else {
					day.hide();
					night.show();
	}
})(jQuery);
